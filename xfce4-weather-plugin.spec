%global minorversion 0.11
%global xfceversion 4.16

Name:           xfce4-weather-plugin
Version:        0.11.2
Release:        1
Summary:        Weather information plugin for the Xfce panel
License:        BSD
URL:            http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:        http://archive.xfce.org/src/panel-plugins/%{name}/%{minorversion}/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gcc-c++
BuildRequires:  libxfce4ui-devel >= %{xfceversion}
BuildRequires:  xfce4-panel-devel >= %{xfceversion}
BuildRequires:  libsoup-devel >= 2.26.0
BuildRequires:  upower-devel >= 0.9.0
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  libxml2-devel >= 2.4.0
BuildRequires:  json-c-devel
Requires:       xfce4-panel >= %{xfceversion}

%description
The weather plugin displays information about the current weather according
your timezone and settings. It allows one to search weather location code in
the same plugin and displays weather status in little icons.

Features include:
 - Temperature, atmospheric pressure and state.
 - Wind speed, gust, and direction.
 - Humidity, Visibility, Dew-point, UV Index.

%prep
%autosetup

%build
%configure
%make_build

%install
%make_install

# remove la file
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# make sure debuginfo is generated properly
chmod -c +x %{buildroot}%{_libdir}/xfce4/panel/plugins/*.so

%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%doc AUTHORS ChangeLog README
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/xfce4/weather

%changelog
* Tue Mar 12 2024 misaka00251 <liuxin@iscas.ac.cn> - 0.11.2-1
- Update to 0.11.2

* Fri Jun 18 2021 zhanglin <lin.zhang@turbolinux.com.cn> - 0.11.0-1
- Update to 0.11.0

* Mon Jul 27 2020 Dillon Chen <dillon.chen@turbolinux.com.cn> - 0.10.1-1
- Init package
